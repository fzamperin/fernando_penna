/*
 * This function is responsible to hide the correct li
 * and then change the class selected
 */ 
const changeArticle = function(index) {
  let listItems = document.getElementsByTagName('li');
  document.getElementsByClassName('selected')[0].classList.remove('selected');
  listItems[index].classList.add('selected');
  let articles = document.getElementsByTagName('article');
  for(let counterArticles = 0; counterArticles < articles.length; counterArticles++) {
    let article = articles[counterArticles];
    if(!article.hidden) {
      article.hidden = true;
      break;
    }
  }
  articles[index].hidden = false;
}

/*
 * editInfo is to create the content editable
 * of the info about the profile
 */ 
const editInfo = function() {
  if(document.getElementById('name').contentEditable === 'true') {
    document.getElementById('name').contentEditable = 'false';
    document.getElementById('website').contentEditable = 'false';
    document.getElementById('phone').contentEditable = 'false';
    document.getElementById('location').contentEditable = 'false';
  }
  else {
    document.getElementById('name').contentEditable = 'true';
    document.getElementById('website').contentEditable = 'true';
    document.getElementById('phone').contentEditable = 'true';
    document.getElementById('location').contentEditable = 'true';
  }
}

module.exports = {
  changeArticle: changeArticle,
  editInfo: editInfo
}