# One Page Application

One page application made for Cyza test.

## Getting Started

First you have to clone the project and inside the project folder run npm:

```
git clone https://fzamperin@bitbucket.org/fzamperin/fernando_penna.git
npm install
```

### Prerequisites

First install nodejs and then install gulp

```
npm install -g gulp
```

Execute both tasks as a part for building the project:

```
gulp fonts
gulp sass
```

Then run the webpack npm run

```
npm run webpack
```