const gulp = require('gulp');
const sass = require('gulp-sass');
const sourcemaps = require('gulp-sourcemaps');
const path = require("path");


/**
 * Executa a task 'sass' ao ser iniciado e 
 * sempre que um arquivo .scss é alterado
 */
gulp.task('watch:sass', ['sass'], function () {
  return gulp.watch(['src/sass/*.scss'], ['sass']);
});

/**
 * Complia os arquivos SASS, 
 * compactados e com 'source map'.
 * Ignora os arquivos iniciados com '_'
 */
gulp.task('sass', function () {
  return gulp.src(['src/sass/*.scss'])
    .pipe(sourcemaps.init())
    .pipe(sass({ outputStyle: 'compressed' }).on('error', sass.logError))
    .pipe(sourcemaps.write('./maps'))
    .pipe(gulp.dest('public/stylesheets/'))
});

gulp.task('fonts', function () {
  return gulp.src(['src/fonts/*.ttf'])
    .pipe(gulp.dest('public/fonts/'))
})

gulp.task('watch', ['watch:sass']);

/**
 * Executas as tasks mínimas para configurar um projeto executável
 */
gulp.task('default', ['sass']);